use std::collections::HashMap;
use personlist::PersonList;

pub struct BasicPersonList {
	data: HashMap<String, BasicContent>,
}

struct BasicContent {
	pub content: Option<String>,
	pub email: String,
}

impl PersonList for BasicPersonList {
	fn add_person (&mut self, email: String) {
		if !self.data.contains_key(&email) {
			self.data.insert(email, BasicContent::new());
		}
	}

	fn rm_person (&mut self, email: String) {
		if self.data.contains_key(&email) {
			self.data.remove(&email);
		}
	}

	fn get_content (&self, email: String) -> Option<String> {
		if let Some(entry) = self.data.get(&email) {
			entry.content.clone()
		}
		else { 
			None 
		}
	}

	fn update_content (&mut self, email: String, content: String) {
		if let Some(entry) = self.data.get_mut(&email) {
			entry.content = Some(content);
		}
	}

	fn updated (&self, email: String) -> bool {
		self.data.get(&email).is_some()
	}

	fn refresh (&mut self) {
		for (_, entry) in self.data.iter_mut() {
			*entry = BasicContent::new();
		}
	}

	fn get_unupdated_email (&self) -> Vec<String>{
		return self.data.values()
			.filter(|entry| entry.content == None )
			.map(|entry| entry.email.clone())
			.collect();
	}
}

impl BasicPersonList {
	pub fn new() -> BasicPersonList {
		BasicPersonList {
			data: HashMap::new(),
		}
	}
}

impl BasicContent {
	fn new() -> BasicContent {
		BasicContent {
			content: None,
			email: String::new(),
		}
	}
}

#[test]
fn test1 () {
	let mut list = BasicPersonList::new();
	list.add_person("Test1".to_string());
	list.add_person("Test2".to_string());
	list.update("Test2".to_string(), "Content ..".to_string());
	assert_eq!(list.get_content("Test1".to_string()), None);
	assert_eq!(list.get_content("Test2".to_string()), Some("Content ..".to_string()));
}
