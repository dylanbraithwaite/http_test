#[cfg(not(test))]
pub mod postgres;

#[cfg(test)]
mod test;

pub trait PersonList {
	fn add_person (&mut self, email: String);
	fn rm_person (&mut self, email: String);
	fn get_content(&self, email: String) -> Option<String>;
	fn update_content (&mut self, email: String, content: String);
	fn updated (&self, email: String) -> bool;
	fn refresh (&mut self);
	fn get_unupdated_email (&self) -> Vec<String>;
}

