mod mockpersonlist;

use std::thread;
use std::mem::forget;
use hyper;
use server;
use self::mockpersonlist::MockPersonList;
use std::io::Read;

fn start_server() {
	let serv = server::Server::new(MockPersonList::new());
	hyper::Server::http(serv).listen("127.0.0.1:3001").unwrap();
}

#[test]
fn test_server_put () {
	thread::spawn(start_server);
	let addr = "http://127.0.0.1:3001";
	let mut client = hyper::Client::new();

	client.put(addr).body("test1@test.test").send().unwrap();
	client.put(addr).body("test2@test.test").send().unwrap();
	client.put(addr).body("test3@test.test").send().unwrap();
	client.put(addr).body("test4@test.test").send().unwrap();
}

#[test]
fn test_server_get () {
	thread::spawn(start_server);
	let addr = "http://127.0.0.1:3001";
	let mut client = hyper::Client::new();

	let mut buf = String::new();
	let mut res = client.get(addr).body("test123boo").send().unwrap();
	res.read_to_string(&mut buf).unwrap();
	assert_eq!(buf, "The\nQuick\nBrown\nFox\n");

}

#[test]
fn test_server_delete () {
	let serv = server::Server::new(MockPersonList::new());
//	let something = hyper::Server::http(serv).listen("127.0.0.1:3001").unwrap();
	//thread::spawn(start_server);

	let addr = "http://127.0.0.1:3001";
	let mut client = hyper::Client::new();

	client.delete(addr).body("test2@test.test").send().unwrap();
	client.delete(addr).body("test4@test.test").send().unwrap();
	unsafe { forget(something) }
}

#[test]
fn test_server_post () {
	thread::spawn(start_server);
	let addr = "http://127.0.0.1:3001";
	let mut client = hyper::Client::new();

	client.post(addr).body("test1@test.test Lorem ipsum dolor sit amet").send().unwrap();
	client.post(addr).body("test4@test.test The quick brown fox jumps over the lazy dog").send().unwrap();

}
