use personlist::PersonList;

pub struct MockPersonList {
	added_emails: u8,
	added_content: u8,
	deleted_emails: u8,
}

impl MockPersonList {
	pub fn new() -> MockPersonList {
		MockPersonList {
			added_emails: 0u8,
			added_content: 0u8,
			deleted_emails: 0u8,
		}
	}
}

impl PersonList for MockPersonList {
	fn add_person (&mut self, email: String) {
		match self.added_emails {
			0 => {assert_eq!(email, "test1@test.test".to_string()); self.added_emails+=1;},
			1 => {assert_eq!(email, "test2@test.test".to_string()); self.added_emails+=1;},
			2 => {assert_eq!(email, "test3@test.test".to_string()); self.added_emails+=1;},
			3 => {assert_eq!(email, "test4@test.test".to_string()); self.added_emails+=1;},
			_ => panic!(),
		}
	}

	fn rm_person (&mut self, email: String) {
		match self.deleted_emails {
			0 => {assert_eq!(email, "test2@test.test".to_string()); self.deleted_emails+=1;},
			1 => {assert_eq!(email, "test4@test.test".to_string()); self.deleted_emails+=1;},
			_ => panic!(),
		}
	}

	fn get_content(&self, email: String) -> Option<String> {None}

	fn update_content(&mut self, email: String, content: String) {
		match self.added_content {
			0 => {
				assert_eq!(email, "test1@test.test".to_string());
				assert_eq!(content, "Lorem ipsum dolor sit amet".to_string());
				self.added_content+=1;
			},
			1 => {
				assert_eq!(email, "test4@test.test".to_string());
				assert_eq!(content, "The quick brown fox jumps over the lazy dog".to_string());
				self.added_content+=1;
			},
			_ => panic!(),
		}
	}

	fn updated (&self, email: String) -> bool { false }

	fn refresh (&mut self) {}

	fn get_unupdated_email (&self) -> Vec<String> {
		return vec!["The".to_string(), "Quick".to_string(), "Brown".to_string(), "Fox".to_string()];
	}
}
