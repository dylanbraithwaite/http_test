use personlist::PersonList;
use postgres::{Connection, SslMode};

pub struct PostgresPersonList {
	connection: Connection,
}

impl PersonList for PostgresPersonList {
	fn add_person (&mut self, email: String) {
		self.connection.execute("INSERT INTO people (email) VALUES ($1)", &[&email]).ok();
	}

	fn rm_person (&mut self, email: String) {
		self.connection.execute("DELETE FROM people WHERE email=$1", &[&email]).ok();
	}

	fn get_content (&self, email: String) -> Option<String> {
		self.connection.prepare("SELECT FROM people WHERE email=$1").ok()
			.and_then(|stmt| stmt.query(&[&email]).ok()
			.and_then(|query| query.iter().next()
			.and_then(|rows| rows.get(0))))
	}

	fn update_content (&mut self, email: String, content: String) {
		self.connection.execute("UPDATE people SET content=$1 WHERE email=$2", &[&content, &email]).ok();
	}

	fn updated (&self, email: String) -> bool {
		let response = self.connection.prepare("SELECT FROM people WHERE email=$1 AND content IS NOT NULL").ok()
			.and_then(|stmt| stmt.query(&[&email]).ok()
			.map(|query| query.iter().next().is_some()));
		if let Some(result) = response {
			result
		}
		else { 
			false 
		}
	}

	fn refresh (&mut self) {
		self.connection.execute("UPDATE people SET content=NULL", &[]).ok();
	}

	fn get_unupdated_email (&self) -> Vec<String> {
		let response = self.connection.prepare("SELECT email FROM people WHERE content IS NULL").ok()
			.and_then(|stmt| stmt.query(&[]).ok()
			.map(|query| query.iter()
			.map(|row| row.get(0)).collect()));
		if let Some(result) = response {
			result
		}
		else {
			Vec::new()
		}
	}
}

impl  PostgresPersonList {
	pub fn new(url: &str) -> PostgresPersonList {
		PostgresPersonList {
			connection: Connection::connect(url, &SslMode::None).unwrap(),
		}
	}
}
