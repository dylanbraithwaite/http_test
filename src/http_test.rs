extern crate hyper;

#[cfg(not(test))] 
extern crate postgres;

mod server;
mod personlist;

#[cfg(not(test))] 
use server::Server;

#[cfg(not(test))] 
use personlist::postgres::PostgresPersonList;

#[cfg(not(test))] 
fn main () {
	let ser = Server::new(PostgresPersonList::new("postgres://postgres_test:test123@127.0.0.1"));
	hyper::Server::http(ser).listen("0:3000").unwrap();
}
