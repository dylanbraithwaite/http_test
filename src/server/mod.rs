use std::io::{Read, Write};
use std::sync::{Arc, Mutex};

use hyper::server::{Request, Response, Handler};
use hyper::method::Method::*;
use hyper::net::Fresh;

use personlist::{PersonList};

pub struct Server<T: PersonList + Send> {
	person_list: Arc<Mutex<T>>,
}

impl<T: PersonList + Send> Handler for Server<T> {
	fn handle(&self, mut req: Request, res: Response<Fresh>) {

		let mut buf = String::new();
		if req.read_to_string(&mut buf).is_err() {
			return;
		}

		let person_list_mutex = self.person_list.clone();
		let mut person_list = match person_list_mutex.lock() {
			Ok(lock_guard) => lock_guard,
			Err(_) => return,
		};

		match req.method{
			Post => {
				let parts: Vec<&str> = buf.splitn(2, |c| { c == ' ' }).collect();
				person_list.update_content(parts[0].to_string(), parts[1].to_string());
			},
			Get => {
				let mut res_content = String::new();
				for email in person_list.get_unupdated_email().iter() {
					res_content.push_str(email);
					res_content.push_str("\n");
				}
				let res = res.start();
				res.and_then(|mut res| 
				 	res.write_all(res_content.as_bytes())
					   .and(res.end())
				).ok();
			},
			Put    => person_list.add_person(buf.clone()),
			Delete => person_list.rm_person(buf.clone()),
			_      => (),
		}
	}
}

impl<T: PersonList + Send> Server<T> {
	pub fn new (person_list: T) -> Server<T> {
		Server {
			person_list: Arc::new(Mutex::new(person_list)),
		}
	}
}

